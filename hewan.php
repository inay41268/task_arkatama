<?php
class hewan{
    var $nama_hewan;
    var $jumlah_kaki;
    var $jenis;

    public function __construct ($nama_hewan, $jumlah_kaki, $jenis)
    {
        $this->nama_hewan = $nama_hewan;
        $this->jumlah_kaki = $jumlah_kaki;
        $this->jenis = $jenis;
    }

    // public function kaki($jumlah_kaki)
    // {
    //     $this->kaki = $jumlah_kaki;
    // }

    // public function jenis($jenis)
    // {
    //     $this->jenis = $jenis;
    // }

    public function bunyi($bunyi)
    {
        $this->bunyi = $bunyi;
    }
}

$sapi = new hewan("Sapi" , "4", "Mamalia");
$sapi->bunyi("Moooo");
// $sapi->kaki = "4";
// $sapi->jenis = "Mamalia";
// $sapi->bunyi = "Mooo";

echo $sapi->nama_hewan;
echo $sapi->jumlah_kaki; 
echo $sapi->jenis; 
echo $sapi->bunyi;

?>