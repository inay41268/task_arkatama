<?php
class persegi{

    public function __construct($sisi)
    {
        $this->sisi = $sisi; 
    }
    public function luas($sisi){
        $hasil = $sisi * $sisi;
        return $hasil;
    }
    public function keliling($sisi){
        $hasil = 4 * $sisi;
        return $hasil;
    }
}

$persegi = new persegi("5");
echo $persegi->luas("5");
echo "$x <br>";
echo $persegi->keliling("5");

class persegi_panjang{
    public function __construct($panjang, $lebar)
    {
        $this->panjang = $panjang; 
        $this->lebar = $lebar;
    }
    public function luas($panjang, $lebar){
        $hasil = $panjang * $lebar;
        return $hasil;
    }
    public function keliling($panjang, $lebar){
        $hasil = 2 * ($panjang + $lebar);
        return $hasil;
    }
}
class segitiga{
    public function __construct($tinggi, $alas)
    {
        $this->tinggi = $tinggi; 
        $this->alas = $alas; 
    }
    public function luas($tinggi, $alas){
        $hasil = $alas/2 * $tinggi;
        return $hasil;
    }
    public function keliling($sisi1, $sisi2, $sisi3){
        $hasil = $sisi1 + $sisi2 + $sisi3;
        return $hasil;
    }    
}
class lingkaran{
    public function __construct($r)
    {
        $this->jari_jari = $r; 
    }
    public function luas($r){
        $hasil = 22/7 * $r *$r;
        return $hasil;
    }
    public function keliling($r){
        $hasil = 4 * 22/7 *$r;
        return $hasil;
    }    
}
class trapesium{
    public function __construct($sisi_atas, $sisi_bawah, $sisi_kanan, $sisi_kiri,$tinggi)
    {
        $this->sisi_atas = $sisi_atas; 
        $this->sisi_bawah = $sisi_bawah;
        $this->sisi_kanan = $sisi_kanan; 
        $this->sisi_kiri = $sisi_kiri;
        $this->tinggi = $tinggi;
    }
    public function luas($sisi_atas, $sisi_bawah, $tinggi){
        $hasil = 1/2 * ($sisi_atas + $sisi_bawah) *$tinggi;
        return $hasil;
    }
    public function keliling($sisi_atas, $sisi_bawah, $sisi_kanan, $sisi_kiri){
        $hasil = $sisi_atas + $sisi_bawah + $sisi_kanan + $sisi_kiri;
        return $hasil;
    }    
}
class jajar_genjang{
    public function __construct($sisi_alas, $sisi_samping, $tinggi)
    {
        $this->sisi_alas = $sisi_alas; 
        $this->sisi_samping = $sisi_samping; 
        $this->tinggi = $tinggi; 
    }
    public function luas($sisi_alas, $tinggi){
        $hasil = $sisi_alas * $tinggi;
        return $hasil;
    }
    public function keliling($sisi_alas, $sisi_samping){
        $hasil =2 * ($sisi_alas + $sisi_samping);
        return $hasil;
    }    
}
class belah_ketupat{
    public function __construct($sisi, $d1, $d2)
    {
        $this->sisi = $sisi; 
        $this->d1 = $d1; 
        $this->d2 = $d2; 
    }
    public function luas($d1, $d2){
        $hasil = 1/2 * $d1 * $d2;
        return $hasil;
    }
    public function keliling($sisi){
        $hasil = 4 * $sisi;
        return $hasil;
    }
    
}

?>