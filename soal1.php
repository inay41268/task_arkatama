<?php

class CatFish{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "CatFish Bisa Terbang";
        }else{
            echo "CatFish Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "CatFish Bisa Berenang";
        }else{
            echo "CatFish Tidak Bisa Berenang";
        }
    } 

}

$catfish = new CatFish("0","Omnivora","Ingsang","Tumbuhan/Serangga","Tidak Berlari",true, false,"Tidak Menangis");

echo $catfish->numberOfLegs;
echo "$x <br>";
echo $catfish->foodClassification;
echo "$x <br>";
echo $catfish->respirationOrgan;
echo "$x <br>";
echo $catfish->eat;
echo "$x <br>";
echo $catfish->run;
echo "$x <br>";
$catfish->canSwim();
echo "$x <br>";
$catfish->canFly();
echo "$x <br>";
echo $catfish->cry;


class BettaFish{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "BettaFish Bisa Terbang";
        }else{
            echo "BettaFish Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "BettaFish Bisa Berenang";
        }else{
            echo "BettaFish Tidak Bisa Berenang";
        }
    } 

}
$BettaFish = new BettaFish("0","Omnivora","Ingsang","Tumbuhan/Serangga","Tidak Berlari","Berenang","Tidak Terbang","Tidak Menangis");

// echo $BettaFish->numberOfLegs;
// echo "$x <br>";
// echo $BettaFish->foodClassification;
// echo "$x <br>";
// echo $BettaFish->respirationOrgan;
// echo "$x <br>";
// echo $BettaFish->eat;
// echo "$x <br>";
// echo $BettaFish->run;
// echo "$x <br>";
// echo $BettaFish->swim;
// echo "$x <br>";
// echo $BettaFish->fly;
// echo "$x <br>";
// echo $BettaFish->cry;
// echo "$x <br>";
// $BettaFish->canFly(false);
// echo "$x <br>";
// $BettaFish->canSwim(true);
// echo "$x <br>";

class Crocodile{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Crocodile Bisa Terbang";
        }else{
            echo "Crocodile Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Crocodile Bisa Berenang";
        }else{
            echo "Crocodile Tidak Bisa Berenang";
        }
    } 
}

$Crocodile = new Crocodile("4","Karnivora","Paru-Paru","Daging","Berlari","Berenang","Tidak Terbang","Tidak Menangis");
// $Crocodile->canFly(false);
// echo "$x <br>";
// $Crocodile->canSwim(true);
// echo "$x <br>";

class Aligator{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Aligator Bisa Terbang";
        }else{
            echo "Aligator Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Aligator Bisa Berenang";
        }else{
            echo "Aligator Tidak Bisa Berenang";
        }
    } 

}

$Aligator = new Aligator("4","Karnivora","Paru-Paru","Daging","Berlari","Berenang","Tidak Terbang","Tidak Menangis");
// $Aligator->canFly(false);
// echo "$x <br>";
// $Aligator->canSwim(true);
// echo "$x <br>";

class Lizard{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Lizard Bisa Terbang";
        }else{
            echo "Lizard Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Lizard Bisa Berenang";
        }else{
            echo "Lizard Tidak Bisa Berenang";
        }
    } 

}

$Lizard = new Lizard("4","Omnivora","Paru-Paru","Tumbuhan/Serangga","Berlari","Tidak Berenang","Tidak Terbang","Tidak Menangis");
// $Lizard->canFly(false);
// echo "$x <br>";
// $Lizard->canSwim(false);
// echo "$x <br>";

class Snake{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Snake Bisa Terbang";
        }else{
            echo "Snake Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Snake Bisa Berenang";
        }else{
            echo "Snake Tidak Bisa Berenang";
        }
    } 

}
$snake = new snake("0","Karnivora","Paru-Paru","Daging","Berlari","Tidak Berenang","Tidak Terbang","Tidak Menangis");
// $snake->canFly(false);
// echo "$x <br>";
// $snake->canSwim(false);
// echo "$x <br>";

class Swan{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Swan Bisa Terbang";
        }else{
            echo "Swan Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Swan Bisa Berenang";
        }else{
            echo "Swan Tidak Bisa Berenang";
        }
    } 

}

$Swan = new Swan("2","Herbivora","Paru-Paru","Tumbuhan","Berlari","Tidak Berenang","Terbang","Tidak Menangis");
// $Swan->canFly(false);
// echo "$x <br>";
// $Swan->canSwim(false);
// echo "$x <br>";

class Duck{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Duck Bisa Terbang";
        }else{
            echo "Duck Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Duck Bisa Berenang";
        }else{
            echo "Duck Tidak Bisa Berenang";
        }
    } 

}

$Duck = new Duck("2","Omnivora","Ingsang","Tumbuhan/Serangga","Berlari","Tidak Berenang","Terbang","Tidak Menangis");
// $Duck->canFly(false);
// echo "$x <br>";
// $Duck->canSwim(false);
// echo "$x <br>";

class Human{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Human Bisa Terbang";
        }else{
            echo "Human Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Human Bisa Berenang";
        }else{
            echo "Human Tidak Bisa Berenang";
        }
    } 

}

$Human = new Human("2","Omnivora","Paru-Paru","Segala","Berlari","Berenang","Tidak Terbang","Menangis");
// $Duck->canFly(false);
// echo "$x <br>";
// $Duck->canSwim(true);
// echo "$x <br>";

class Whale{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Whale Bisa Terbang";
        }else{
            echo "Whale Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Whale Bisa Berenang";
        }else{
            echo "Whale Tidak Bisa Berenang";
        }
    } 

}

$Whale = new Whale("0","Omnivora","Paru-Paru","Daging","Tidak Berlari","Berenang","Tidak Terbang","Tidak Menangis");
// $Whale->canFly(false);
// echo "$x <br>";
// $Whale->canSwim(true);
// echo "$x <br>";

class Dolphine{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Dolphine Bisa Terbang";
        }else{
            echo "Dolphine Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Dolphine Bisa Berenang";
        }else{
            echo "Dolphine Tidak Bisa Berenang";
        }
    } 

}

$Dolphine = new Dolphine("0","Omnivora","Ingsang","Segala","Tidak Berlari","Berenang","Tidak Terbang","Tidak Menangis");


class Bat{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Bat Bisa Terbang";
        }else{
            echo "Bat Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Bat Bisa Berenang";
        }else{
            echo "Bat Tidak Bisa Berenang";
        }
    } 

}

$Bat = new Bat("2","Omnivora","Paru-Paru","Segala","Tidak Berlari","Tidak Berenang","Terbang","Tidak Menangis");


class Tiger{
    public function __construct ($numberOfLegs, $foodClassification, $respirationOrgan, $eat, $run, $swim, $fly, $cry)
    {
        $this->numberOfLegs = $numberOfLegs;     
        $this->foodClassification = $foodClassification;
        $this->respirationOrgan = $respirationOrgan;
        $this->eat = $eat;
        $this->run = $run;
        $this->swim = $swim;
        $this->fly = $fly;
        $this->cry = $cry;
    }
    
    public function canFly(){ 
        if ($this->fly==true)
        {
            echo "Tiger Bisa Terbang";
        }else{
            echo "Tiger Tidak Bisa Terbang";
        }
    }
    public function canSwim(){    
        if($this->swim==true)
        {
            echo "Tiger Bisa Berenang";
        }else{
            echo "Tiger Tidak Bisa Berenang";
        }
    } 
    

}
$Tiger = new Tiger("4","Karnivora","Paru-Paru","Daging","Berlari","Tidak Berenang","Tidak Terbang","Tidak Menangis");

?>